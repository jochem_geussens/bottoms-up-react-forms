import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withKnobs, text, number} from '@storybook/addon-knobs/react';
import {InputText} from "./components/InputText/InputText";
import {ResetPasswordForm} from "./ResetPasswordForm/ResetPasswordForm";
import {ShoppingList} from "./ShoppingList/ShoppingList";
import {minimum, validatorBuilder} from "../src/helpers/Validators";
import {ShoppingListWithDescription} from "./ShoppingList/ShoppingListWithDescription";
import {TodoList} from "./TodoList/TodoList";
import {UserProfile} from "./UserProfileForm/UserProfile";

storiesOf('Input components/Input text', module)
    .addDecorator(withKnobs)
    .add('Full example', () => <div>
        <InputText {...{
            label: text('Label', 'My text input field'),
            value: text('Value', 'Test value'),
            initialValue: text('Initial value', 'Test value'),
            name: text('Name for objects', 'Test name'),
            index: number('Index for arrays', 1),
            validator: validatorBuilder(minimum(5)),
            onChange: action('onChange'),
            onFocus: action('onFocus'),
            onBlur: action('onBlur'),
        }}/>

        <InputText {...{
            label: 'Trigger focus/blur events'
        }}/>
    </div>);
storiesOf('Form', module)
    .add('User profile form', () => <UserProfile/>)
    .add('Reset password form', () => <ResetPasswordForm/>)
    .add('Shopping list form', () => <ShoppingList/>)
    .add('Shopping list with description form', () => <ShoppingListWithDescription/>)
    .add('Todo list form', () => <TodoList/>);