/* eslint react/prop-types: 0 */
import React, {PureComponent} from 'react';
import {InputManager} from "../../../src/components/InputManager";
import styles from './InputText.module.css';

export class InputText extends PureComponent {
    render() {
        return <InputManager {...{...this.props}}>
            {(input) => {
                const {error, touched, focus, value, onChange, onFocus, onBlur, label, type} = input;
                const showError = error && touched && !focus;

                return <div {...{className: styles.input}}>
                    <label {...{className: styles.label}}>{label}
                        <input {...{
                            type,
                            value: value || '',
                            onChange: (event) => onChange(event.target.value),
                            onFocus,
                            onBlur
                        }}/>
                        {showError ? <label {...{className: styles.error}}>{error}</label> : null}
                    </label>
                </div>
            }}
        </InputManager>;
    }
}

InputText.defaultProps = {
    type: 'text'
};