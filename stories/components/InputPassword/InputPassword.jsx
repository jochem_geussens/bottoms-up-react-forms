import React, {PureComponent} from 'react';
import {required, passwordStrength, maximum, validatorBuilder} from "../../../src/helpers/Validators";
import {InputText} from '../InputText/InputText';

export class InputPassword extends PureComponent {
    render() {
        return <InputText {...this.props}/>;
    }
}

const MEDIUM_PASSWORD_REGEX = /^.*((?=.*\d)(?=.*[a-zA-Z])|(?=.{8,})(?=.*[a-zA-Z])|(?=.{8,})(?=.*\d)).*$/;
const STRONG_PASSWORD_REGEX = /^.*(?=.{8,})(?=.*\d)(?=.*[a-zA-Z]).*$/;
InputPassword.defaultProps = {
    type: 'password',
    validator: validatorBuilder(required(), passwordStrength(MEDIUM_PASSWORD_REGEX, STRONG_PASSWORD_REGEX), maximum(50))
};