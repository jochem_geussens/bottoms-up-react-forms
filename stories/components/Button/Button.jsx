/* eslint react/prop-types: 0 */
import React, {Component} from 'react';
import './button.css';

export class Button extends Component {
    render() {
        const {children, label, enable, ...buttonProps} = this.props;
        return <button {...{...buttonProps, disabled: !enable}}>{label || children}</button>;
    }
}

Button.defaultProps = {
    type: 'button',
    enable: true
};