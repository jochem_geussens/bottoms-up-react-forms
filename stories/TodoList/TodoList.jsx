/* eslint react/prop-types: 0 */
/* eslint react/jsx-key: 0 */
import React, {Fragment, Component} from "react";
import {Button} from "../components/Button/Button";
import {InputGroupManager} from "../../src/components/InputGroupManager";
import styles from "./TodoList.module.css";
import {InputText} from "../components/InputText/InputText";

export class TodoItem extends Component {
    constructor(props) {
        super(props);
        this.onMoveUp = this.onMoveUp.bind(this);
        this.onMoveDown = this.onMoveDown.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    onMoveUp() {
        const {index, arrayHelper} = this.props;
        arrayHelper.swap(index, index - 1);
    }

    onMoveDown() {
        const {index, arrayHelper} = this.props;
        arrayHelper.swap(index, index + 1);
    }

    onDelete() {
        const {index, arrayHelper} = this.props;
        arrayHelper.delete(index);
    }

    render() {
        const {props, onMoveUp, onDelete, onMoveDown} = this;
        const {arrayHelper, ...otherProps} = props;
        return <div className={styles.todoListItem}>
            <InputText {...{...otherProps}}/>
            <Button {...{label: "Move up", onClick: onMoveUp}}/>
            <Button {...{label: "Move down", onClick: onMoveDown}}/>
            <Button {...{label: "Delete", onClick: onDelete}}/>
        </div>;
    }
}

export class TodoList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: []
        };
        this.onChange = this.onChange.bind(this);
    }

    onChange(newState) {
        this.setState(newState);
    }

    render() {
        return <InputGroupManager {...{...this.state, onChange: this.onChange}}>
            {form => <Fragment>
                {form.value.map((value, index) => <TodoItem {...{
                    ...form.helpers.buildProps({index}),
                    arrayHelper: form.helpers.array
                }}/>)}
                <Button{...{
                    label: "Add empty Todo item",
                    enable: form.valid,
                    onClick: () => form.helpers.array.add({value: "New TODO item"})
                }}/>
            </Fragment>}
        </InputGroupManager>;
    }
}
