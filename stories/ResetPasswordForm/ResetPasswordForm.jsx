/* eslint react/prop-types: 0 */
import React, {Component, Fragment} from "react";
import {InputPassword} from "../components/InputPassword/InputPassword";
import {defaultReducer, validatorBuilder} from "../../src";
import {InputGroupManager} from "../../src/components/InputGroupManager";
import {Button} from "../components/Button/Button";
import {FormResetter} from "../../src/helpers/FormResetter";
import styles from "./ResetPasswordForm.module.css";

function defaultAndEnsureNewPasswordsEquality({childStates, initialValue, name}) {
    const reduceResult = defaultReducer({childStates, initialValue, name});
    reduceResult.formError = undefined;
    if (reduceResult.valid && childStates.newPassword && childStates.repeatNewPassword && childStates.newPassword.value !== childStates.repeatNewPassword.value) {
        reduceResult.valid = false;
        reduceResult.error = "passwordNotEqual";
    }
    return reduceResult;
}

export class ResetPasswordForm extends Component {
    state = {
        value: {}
    };

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.clearForm = this.clearForm.bind(this);
        this.formResetter = new FormResetter(this);
    }

    onChange(formManager) {
        this.setState(formManager);
    }

    resetForm() {
        this.clearForm();
        this.formResetter.resetForm();
    }

    clearForm() {
        this.setState({value: {}});
    }

    render() {
        const {state, onChange, resetForm, clearForm} = this;
        return <InputGroupManager {...{
            ...state,
            ...this.formResetter.toProps(),
            onChange,
            reducer: defaultAndEnsureNewPasswordsEquality
        }}>
            {(form) => {
                return <Fragment>
                    <InputPassword {...{
                        ...form.helpers.buildProps({name: "oldPassword"}),
                        label: "Old password",
                        validator: validatorBuilder() // Disable the actual password validator
                    }}/>
                    <InputPassword {...{
                        ...form.helpers.buildProps({name: "newPassword"}),
                        label: "New password"
                    }}/>
                    <InputPassword {...{
                        ...form.helpers.buildProps({name: "repeatNewPassword"}),
                        label: "Repeat new password"
                    }}/>
                    {!!form.error && !form.focus && <p className={styles.formError}>{form.error}</p>}
                    <Button{...{
                        label: "Reset password",
                        onClick: () => form.valid && alert(JSON.stringify(form.value))
                    }}/>
                    <Button{...{
                        label: "Reset form",
                        onClick: resetForm
                    }}/>
                    <Button{...{
                        label: "Clear form",
                        onClick: clearForm
                    }}/>
                </Fragment>;
            }}
        </InputGroupManager>;
    }
}