/* eslint react/prop-types: 0 */
/* eslint react/jsx-key: 0 */
import React, {Component, Fragment} from "react";
import {InputGroupManager} from "../../src/components/InputGroupManager";
import {Button} from "../components/Button/Button";
import {InputText} from "../components/InputText/InputText";


function initialValue() {
    return {
        firstName: "testFirstName",
        lastName: "testLastName",
        emailAddresses: [
            "main@email.com",
            "other@email.com"
        ],
        addresses: [
            {
                street: "street",
                city: "testCity",
                country: "testCountry"
            }
        ]
    };
}

export class UserProfile extends Component {
    state = {
        value: initialValue(),
        initialValue: initialValue()
    };

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    onChange(formManager) {
        this.setState(formManager);
    }

    render() {
        const {onChange, state} = this;
        const {value, initialValue} = state;
        return <InputGroupManager {...{onChange, value, initialValue}}>
            {(form) => {
                return <Fragment>
                    <InputText {...{label: "First name", ...form.helpers.buildProps({name: "firstName"})}}/>
                    <InputText {...{label: "Last name", ...form.helpers.buildProps({name: "lastName"})}}/>
                    <InputGroupManager {...{valueType: "array", ...form.helpers.buildProps({name: "emailAddresses"})}}>
                        {(emails) => <Fragment>
                            {emails.value.map((email, index) => {
                                return <InputText {...{label: "Email", ...emails.helpers.buildProps({index})}}/>;
                            })}
                            <Button {...{label: "Add Email", onClick: emails.helpers.array.add}}/>
                        </Fragment>}
                    </InputGroupManager>
                    <InputGroupManager {...{valueType: "array", ...form.helpers.buildProps({name: "addresses"})}}>
                        {(addresses) => <Fragment>
                            <h5>My addresses</h5>
                            {addresses.value.map((address, index) => {
                                return <InputGroupManager {...{...addresses.helpers.buildProps({index})}}>
                                    {(address) => <Fragment>
                                        {index > 0 && <br/>}
                                        {index > 0 && <hr/>}
                                        {index > 0 && <br/>}
                                        <InputText {...{label: "Street", ...address.helpers.buildProps({name: "street"})}}/>
                                        <InputText {...{label: "City", ...address.helpers.buildProps({name: "city"})}}/>
                                        <InputText {...{label: "Country", ...address.helpers.buildProps({name: "country"})}}/>
                                    </Fragment>}
                                </InputGroupManager>;
                            })}
                            <Button {...{label: "Add address", onClick: addresses.helpers.array.add}}/>
                        </Fragment>}
                    </InputGroupManager>
                </Fragment>;
            }}
        </InputGroupManager>;
    }
}