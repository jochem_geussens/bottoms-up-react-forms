/* eslint react/prop-types: 0 */
/* eslint react/jsx-key: 0 */
import React, {Component, Fragment} from 'react';
import {Button} from "../components/Button/Button";
import {ShoppingListItem} from "./ShoppingListItem";
import {InputGroupManager} from "../../src/components/InputGroupManager";


function Group(props) {
    const { value, helpers, valid } = props;
    return <Fragment>
        {value.map((value, index) => <ShoppingListItem {...{
            ...helpers.buildProps({index}),
            arrayHelper: helpers.array
        }}/>)}
        <Button{...{
            label: 'Add empty shopping list item',
            enable: valid,
            onClick: () => helpers.array.add({value: 'New shopping list item'})
        }}/>
        <Button{...{
            label: 'Delete all list items',
            enable: value.length,
            onClick: helpers.array.deleteAll
        }}/>
    </Fragment>;
}


export class ShoppingList extends Component {
    state = {
        value: [
            'Apples',
            'Potatoes'
        ]
    };

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    onChange(formManager) {
        this.setState(formManager);
    }

    render() {
        const {props, state, onChange} = this;
        if (props.value) {
            // extension to allow reuse for ShoppingListWithDescription (onChange and value is coming from this.props now)
            return <InputGroupManager {...{
                valueType: 'array',
                Group,
                ...this.props,
            }}/>;
        }
        else {
            return <InputGroupManager {...{
                onChange,
                valueType: 'array',
                Group,
                ...state
            }}/>;
        }
    }
}

ShoppingList.defaultProps = {};

ShoppingList.propTypes = {};