/* eslint react/prop-types: 0 */
import React, {Component, Fragment} from 'react';
import {InputText} from "../components/InputText/InputText";
import {ShoppingList} from "./ShoppingList";
import {InputGroupManager} from "../../src/components/InputGroupManager";

export class ShoppingListWithDescription extends Component {
    state = {
        value: {
            description: 'My groceries',
            shoppingList: [
                'Apples',
                'Potatoes'
            ]
        }
    };

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    onChange(formManager) {
        this.setState(formManager);
    }

    render() {
        const {state, onChange} = this;
        return <InputGroupManager {...{
            onChange,
            ...state
        }}>
            {form => {
                return <Fragment>
                    <InputText {...{...form.helpers.buildProps({name: 'description'}), label: 'Description'}}/>
                    <ShoppingList {...{...form.helpers.buildProps({name: 'shoppingList'})}}/>
                </Fragment>;
            }}
        </InputGroupManager>;
    }
}