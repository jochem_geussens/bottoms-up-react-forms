import React, {Component} from 'react';
import {InputText} from "../components/InputText/InputText";
import {Button} from "../components/Button/Button";
import styles from './ShoppingList.module.css';

export class ShoppingListItem extends Component {
    render() {
        const {props} = this;
        const {value, arrayHelper, index, ...otherProps} = props;
        return <div className={styles.shoppingListItem}>
            <InputText {...{...otherProps, value, index}}/>
            <Button {...{label: 'Move up', onClick: () => arrayHelper.swap(index, index - 1)}}/>
            <Button {...{label: 'Move down', onClick: () => arrayHelper.swap(index, index + 1)}}/>
            <Button {...{label: 'Delete', onClick: () => arrayHelper.delete(index)}}/>
        </div>;
    }
}

ShoppingListItem.defaultProps = {};

ShoppingListItem.propTypes = {};