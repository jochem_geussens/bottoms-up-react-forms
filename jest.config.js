module.exports = {
    "setupTestFrameworkScriptFile": "./setupTests.js",
    "testEnvironment": "node",
    "testURL": "http://www.test.com",
    "snapshotSerializers": [
        "enzyme-to-json/serializer"
    ],
    "collectCoverageFrom": [
        "**/src/**.{js,jsx}",
        "!**/src/testHelpers/**.{js,jsx}",
    ]
};