import React, {Component} from "react";
import isEqual from "fast-deep-equal";
import PropTypes from "prop-types";
import {
    INPUT_MANAGER_INVALID_INPUT_PROPS,
    INPUT_MANAGER_INVALID_VALIDATOR_PROPS,
    INPUT_MANAGER_INVALID_VALIDATOR_RESPONSE
} from "../helpers/ERRORS";
import {ChangeOutput} from "../helpers/ChangeOutput";

/**
 * This class is not a PureComponent because sometimes you want the parent to be able to discard changes of the
 * component, in which case the parent will just do a rerender and pass the same value to the component.
 */
export class InputManager extends Component {
    constructor(props) {
        super(props);
        const {value} = props;
        this.state = {};
        this.state = this.buildState(value);
        this.onChange = this.onChange.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onFocus = this.onFocus.bind(this);
    }

    buildState(value) {
        let validatorResult = {
            valid: false,
            error: "unknown"
        };
        const {initialValue, validator} = this.props;
        if (typeof validator !== "function") {
            throw INPUT_MANAGER_INVALID_VALIDATOR_PROPS;
        }
        validatorResult = validator(value, this.props, this.state);

        if (typeof validatorResult !== "object" || !validatorResult.hasOwnProperty("valid") || !validatorResult.hasOwnProperty("error")) {
            throw INPUT_MANAGER_INVALID_VALIDATOR_RESPONSE;
        }

        const {valid, error} = validatorResult;
        const newState = {
            dirty: !isEqual(initialValue, value),
            focus: this.state.focus,
            touched: this.state.touched,
            value,
            valid,
            error,
            name: this.props.name,
            index: this.props.index
        };

        return new ChangeOutput(newState);
    }

    onChange(value) {
        if (value === this.state.value) {
            if (this.props.debug && this.props.debug.logOnChange) {
                console.log(this.props.name || this.props.index, "onChange", // eslint-disable-line
                    value, "New value equals current value, change not propagated."); // eslint-disable-line
            }
            return;
        }
        const newState = this.buildState(value);
        // TODO use updater function and move the this.props.onChange to the componentDidUpdate
        this.setState(newState);
        if (this.props.debug && this.props.debug.logOnChange) {
            console.log(this.props.name || this.props.index, "onChange", value, newState); // eslint-disable-line
        }
        this.props.onChange(newState);
    }

    onFocus() {
        if (this.state.focus) {
            if (this.props.debug && this.props.debug.logOnFocus) {
                console.log(this.props.name || this.props.index, "onFocus", // eslint-disable-line
                    "Input component already has focus, change not propagated."); // eslint-disable-line
            }
            return;
        }
        if (this.props.debug && this.props.debug.logOnFocus) {
            console.log(this.props.name || this.props.index, "onFocus", this.state); // eslint-disable-line
        }

        // TODO use updater function and move the this.props.onChange to the componentDidUpdate
        this.setState({...this.buildState(this.state.value), focus: true}, () =>
            this.props.onChange(this.state));
    }

    onBlur() {
        if (!this.state.focus && this.state.touched) {
            if (this.props.debug && this.props.debug.logOnBlur) {
                console.log(this.props.name || this.props.index, "onBlur", // eslint-disable-line
                    "Input component does not have focus and was already touched, change not propagated."); // eslint-disable-line
            }
            return;
        }
        if (this.props.debug && this.props.debug.logOnBlur) {
            console.log(this.props.name || this.props.index, "onBlur", this.state); // eslint-disable-line
        }
        // TODO use updater function and move the this.props.onChange to the componentDidUpdate
        this.setState({...this.buildState(this.state.value), focus: false, touched: true}, () =>
            this.props.onChange(this.state));
    }

    componentDidMount() {
        this.props.onChange(this.state); // Notify parents about the validation results etc.
    }

    componentDidUpdate(prevProps, prevState) {
        const componentInitiatedUpdate = this.state.value !== prevState.value;
        const parentInitiatedUpdate = !componentInitiatedUpdate && this.props.value !== this.state.value;
        if (parentInitiatedUpdate) { // Parent changed the value, so a new validation is required
            // TODO use updater function and move the this.props.onChange to the componentDidUpdate
            this.setState(this.buildState(this.props.value), () => {
                this.props.onChange(this.state); // Notify parents about the validation results etc.
            });
        }
    }

    render() {
        const {props, onChange, onBlur, onFocus} = this;
        const {
            Input, children, onChange: ignoreOnChange, initialValue, validator, value, index, name, ...otherProps
        } = props;
        if (this.props.debug && this.props.debug.logOnRender) {
            console.log(name || index, props); // eslint-disable-line
        }

        const propsToChildren = {
            ...otherProps,
            ...this.state,
            onChange,
            onBlur,
            onFocus,
        };
        if (Input) {
            return <Input {...propsToChildren}/>;
        } else if (children) {
            return children(propsToChildren);
        } else {
            throw INPUT_MANAGER_INVALID_INPUT_PROPS;
        }
    }
}

InputManager.defaultProps = {
    value: "",
    name: undefined,
    index: undefined,
    initialValue: "",
    validator: () => {
        return {valid: true, error: false};
    },
    onChange: () => null,
};

InputManager.propTypes = {
    value: PropTypes.any,
    initialValue: PropTypes.any,
    validator: PropTypes.any.isRequired,
    name: PropTypes.any,
    index: PropTypes.number,
    onChange: PropTypes.func,
    debug: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.objectOf({
            logOnChange: PropTypes.bool,
            logOnFocus: PropTypes.bool,
            logOnBlur: PropTypes.bool,
            logOnRender: PropTypes.bool
        })
    ])
};