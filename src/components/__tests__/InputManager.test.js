import {InputManager} from "../InputManager";
import {mockReact} from "../../testHelpers/mockReact";
import {addEvent, clearEvents, events} from "jest-mock-utils";
import {} from "../../helpers/ERRORS";
import {mockConsole} from "../../testHelpers/mockConsole";
import {INPUT_MANAGER_INVALID_INPUT_PROPS} from "../../helpers/ERRORS";

mockReact();
mockConsole();

describe("When an instance of the InputManager is created", () => {
    beforeEach(() => {
        clearEvents();
    });
    describe("constructor() & buildProps()", () => {
        describe("When all props are provided", () => {
            function act() {
                return new InputManager({
                    value: "testValue",
                    validator: () => ({valid: true, error: false})
                });
            }

            test("Then the instance will have these values", () => {
                expect(act()).toEqual({
                    "onChange": expect.any(Function),
                    "onFocus": expect.any(Function),
                    "onBlur": expect.any(Function),
                    "props": {
                        "value": "testValue",
                        "validator": expect.any(Function)
                    },
                    "refs": {},
                    "state": {
                        "dirty": true,
                        "error": false,
                        "focus": false,
                        "touched": false,
                        "valid": true,
                        "value": "testValue"
                    },
                    "updater": expect.anything()
                });
            });
        });
        describe("When the validator is missing", () => {
            function act() {
                return new InputManager({});
            }

            test("Then the following error will be thrown", () => {
                expect(act).toThrow("Error 2: InputManager expects props to contain a function named \"validator\"");
            });
        });
        describe("When the validator does not return valid", () => {
            function act() {
                return new InputManager({
                    value: "testValue",
                    validator: () => ({notValid: true, error: false})
                });
            }

            test("Then the following error will be thrown", () => {
                expect(act).toThrow("Error 3: The validator passed to the InputManager did not return an object containing a \"value\" and \"error\" property");
            });
        });
        describe("When the validator does not return error", () => {
            function act() {
                return new InputManager({
                    value: "testValue",
                    validator: () => ({valid: true, notError: false})
                });
            }

            test("Then the following error will be thrown", () => {
                expect(act).toThrow("Error 3: The validator passed to the InputManager did not return an object containing a \"value\" and \"error\" property");
            });
        });
    });

    describe("onChange()", () => {
        describe("Given valid props and the child returns a valid new value", () => {
            function act() {

                const instance = new InputManager({
                    index: "testIndex",
                    value: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    }
                });

                instance.onChange("newTestValue1");
                instance.mockForceUpdate();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "state": {
                        "index": "testIndex",
                        "dirty": true,
                        "error": false,
                        "focus": false,
                        "touched": false,
                        "valid": true,
                        "value": "newTestValue1"
                    },
                    "events": [
                        {
                            "event": {
                                "nextMockState": {
                                    "index": "testIndex",
                                    "dirty": true,
                                    "error": false,
                                    "focus": false,
                                    "touched": false,
                                    "valid": true,
                                    "value": "newTestValue1"
                                },
                                "stateUpdate": {
                                    "index": "testIndex",
                                    "dirty": true,
                                    "error": false,
                                    "focus": false,
                                    "touched": false,
                                    "valid": true,
                                    "value": "newTestValue1"
                                }
                            },
                            "name": "ReactAddState"
                        },
                        {
                            "event": {
                                "index": "testIndex",
                                "dirty": true,
                                "error": false,
                                "focus": false,
                                "touched": false,
                                "valid": true,
                                "value": "newTestValue1"
                            },
                            "name": "onChange"
                        }
                    ]
                });
            });
        });
        describe("Given valid props and the child returns a valid new value + log onChange", () => {
            function act() {

                const instance = new InputManager({
                    index: "testIndex",
                    value: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    },
                    debug: {
                        logOnChange: true
                    }
                });

                instance.onChange("newTestValue1");
                instance.mockForceUpdate();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "state": {
                        "index": "testIndex",
                        "dirty": true,
                        "error": false,
                        "focus": false,
                        "touched": false,
                        "valid": true,
                        "value": "newTestValue1"
                    },
                    "events": [
                        {
                            "event": {
                                "nextMockState": {
                                    "index": "testIndex",
                                    "dirty": true,
                                    "error": false,
                                    "focus": false,
                                    "touched": false,
                                    "valid": true,
                                    "value": "newTestValue1"
                                },
                                "stateUpdate": {
                                    "index": "testIndex",
                                    "dirty": true,
                                    "error": false,
                                    "focus": false,
                                    "touched": false,
                                    "valid": true,
                                    "value": "newTestValue1"
                                }
                            },
                            "name": "ReactAddState"
                        },
                        {
                            "event": [
                                "testIndex",
                                "onChange",
                                "newTestValue1",
                                {
                                    "dirty": true,
                                    "error": false,
                                    "focus": false,
                                    "index": "testIndex",
                                    "touched": false,
                                    "valid": true,
                                    "value": "newTestValue1"
                                }
                            ],
                            "name": "ConsoleLog"
                        },
                        {
                            "event": {
                                "index": "testIndex",
                                "dirty": true,
                                "error": false,
                                "focus": false,
                                "touched": false,
                                "valid": true,
                                "value": "newTestValue1"
                            },
                            "name": "onChange"
                        }
                    ]
                });
            });
        });
        describe("Given valid props and the child returns the same value", () => {
            function act() {

                const instance = new InputManager({
                    index: "testIndex",
                    value: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    }
                });

                instance.onChange("testValue");
                instance.mockForceUpdate && instance.mockForceUpdate(); // mockForceUpdate not available because no setState occurred.
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "state": {
                        "index": "testIndex",
                        "dirty": true,
                        "error": false,
                        "focus": false,
                        "touched": false,
                        "valid": true,
                        "value": "testValue"
                    },
                    "events": []
                });
            });
        });
        describe("Given valid props and the child returns the same value + log onChange", () => {
            function act() {

                const instance = new InputManager({
                    index: "testIndex",
                    value: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    },
                    debug: {
                        logOnChange: true
                    }
                });

                instance.onChange("testValue");
                instance.mockForceUpdate && instance.mockForceUpdate(); // mockForceUpdate not available because no setState occurred.
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "state": {
                        "index": "testIndex",
                        "dirty": true,
                        "error": false,
                        "focus": false,
                        "touched": false,
                        "valid": true,
                        "value": "testValue"
                    },
                    "events": [{
                        "event": [
                            "testIndex",
                            "onChange",
                            "testValue",
                            "New value equals current value, change not propagated."
                        ],
                        "name": "ConsoleLog"
                    }]
                });
            });
        });
    });

    describe("onFocus()", () => {
        describe("Given that the component is currently not focussed", () => {
            function act() {

                const instance = new InputManager({
                    index: "testIndex",
                    value: "testValue",
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    }
                });

                instance.onFocus();
                instance.mockForceUpdate();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "events": [
                        {
                            "event": {
                                "nextMockState": {
                                    "dirty": false,
                                    "error": false,
                                    "focus": true,
                                    "index": "testIndex",
                                    "touched": false,
                                    "valid": true,
                                    "value": "testValue"
                                },
                                "stateUpdate": {
                                    "dirty": false,
                                    "error": false,
                                    "focus": true,
                                    "index": "testIndex",
                                    "touched": false,
                                    "valid": true,
                                    "value": "testValue"
                                }
                            },
                            "name": "ReactAddState"
                        },
                        {
                            "event": {
                                "dirty": false,
                                "error": false,
                                "focus": true,
                                "index": "testIndex",
                                "touched": false,
                                "valid": true,
                                "value": "testValue"
                            },
                            "name": "onChange"
                        }
                    ],
                    "state": {
                        "dirty": false,
                        "error": false,
                        "focus": true,
                        "index": "testIndex",
                        "touched": false,
                        "valid": true,
                        "value": "testValue"
                    }
                });
            });
        });
        describe("Given that the component is currently not focussed + log onFocus", () => {
            function act() {

                const instance = new InputManager({
                    index: "testIndex",
                    value: "testValue",
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    },
                    debug: {
                        logOnFocus: true
                    }
                });

                instance.onFocus();
                instance.mockForceUpdate();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "events": [
                        {
                            "event": [
                                "testIndex",
                                "onFocus",
                                {
                                    "dirty": false,
                                    "error": false,
                                    "focus": false,
                                    "index": "testIndex",
                                    "touched": false,
                                    "valid": true,
                                    "value": "testValue"
                                }
                            ],
                            "name": "ConsoleLog"
                        },
                        {
                            "event": {
                                "nextMockState": {
                                    "dirty": false,
                                    "error": false,
                                    "focus": true,
                                    "index": "testIndex",
                                    "touched": false,
                                    "valid": true,
                                    "value": "testValue"
                                },
                                "stateUpdate": {
                                    "dirty": false,
                                    "error": false,
                                    "focus": true,
                                    "index": "testIndex",
                                    "touched": false,
                                    "valid": true,
                                    "value": "testValue"
                                }
                            },
                            "name": "ReactAddState"
                        },
                        {
                            "event": {
                                "dirty": false,
                                "error": false,
                                "focus": true,
                                "index": "testIndex",
                                "touched": false,
                                "valid": true,
                                "value": "testValue"
                            },
                            "name": "onChange"
                        }
                    ],
                    "state": {
                        "dirty": false,
                        "error": false,
                        "focus": true,
                        "index": "testIndex",
                        "touched": false,
                        "valid": true,
                        "value": "testValue"
                    }
                });
            });
        });
        describe("Given that the component is currently focused", () => {
            function act() {

                const instance = new InputManager({
                    index: "testIndex",
                    value: "testValue",
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    }
                });

                instance.state.focus = true;
                instance.onFocus();
                instance.mockForceUpdate && instance.mockForceUpdate();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "events": [],
                    "state": {
                        "dirty": false,
                        "error": false,
                        "focus": true,
                        "index": "testIndex",
                        "touched": false,
                        "valid": true,
                        "value": "testValue"
                    }
                });
            });
        });
        describe("Given that the component is currently focused + log onFocus", () => {
            function act() {

                const instance = new InputManager({
                    index: "testIndex",
                    value: "testValue",
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    },
                    debug: {
                        logOnFocus: true
                    }
                });
                instance.state.focus = true;
                instance.onFocus();
                instance.mockForceUpdate && instance.mockForceUpdate();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "events": [
                        {
                            "event": [
                                "testIndex",
                                "onFocus",
                                "Input component already has focus, change not propagated."
                            ],
                            "name": "ConsoleLog"
                        }
                    ],
                    "state": {
                        "dirty": false,
                        "error": false,
                        "focus": true,
                        "index": "testIndex",
                        "touched": false,
                        "valid": true,
                        "value": "testValue"
                    }
                });
            });
        });
    });

    describe("onBlur()", () => {
        describe("Given that the component is currently focused", () => {
            function act() {

                const instance = new InputManager({

                    index: "testIndex",
                    value: "testValue",
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    }
                });

                instance.state.focus = true;
                instance.onBlur();
                instance.mockForceUpdate && instance.mockForceUpdate();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "events": [
                        {
                            "event": {
                                "nextMockState": {
                                    "dirty": false,
                                    "error": false,
                                    "focus": false,
                                    "index": "testIndex",
                                    "touched": true,
                                    "valid": true,
                                    "value": "testValue"
                                },
                                "stateUpdate": {
                                    "dirty": false,
                                    "error": false,
                                    "focus": false,
                                    "index": "testIndex",
                                    "touched": true,
                                    "valid": true,
                                    "value": "testValue"
                                }
                            },
                            "name": "ReactAddState"
                        },
                        {
                            "event": {
                                "dirty": false,
                                "error": false,
                                "focus": false,
                                "index": "testIndex",
                                "touched": true,
                                "valid": true,
                                "value": "testValue"
                            },
                            "name": "onChange"
                        }
                    ],
                    "state": {
                        "dirty": false,
                        "error": false,
                        "focus": false,
                        "index": "testIndex",
                        "touched": true,
                        "valid": true,
                        "value": "testValue"
                    }
                });
            });
        });
        describe("Given that the component is currently focused + log onFocus", () => {
            function act() {

                const instance = new InputManager({

                    index: "testIndex",
                    value: "testValue",
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    },
                    debug: {
                        logOnBlur: true
                    }
                });
                instance.state.focus = true;
                instance.onBlur();
                instance.mockForceUpdate && instance.mockForceUpdate();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "events": [
                        {
                            "event": [
                                "testIndex",
                                "onBlur",
                                {
                                    "dirty": false,
                                    "error": false,
                                    "focus": true,
                                    "index": "testIndex",
                                    "touched": false,
                                    "valid": true,
                                    "value": "testValue"
                                }
                            ],
                            "name": "ConsoleLog"
                        },
                        {
                            "event": {
                                "nextMockState": {
                                    "dirty": false,
                                    "error": false,
                                    "focus": false,
                                    "index": "testIndex",
                                    "touched": true,
                                    "valid": true,
                                    "value": "testValue"
                                },
                                "stateUpdate": {
                                    "dirty": false,
                                    "error": false,
                                    "focus": false,
                                    "index": "testIndex",
                                    "touched": true,
                                    "valid": true,
                                    "value": "testValue"
                                }
                            },
                            "name": "ReactAddState"
                        },
                        {
                            "event": {
                                "dirty": false,
                                "error": false,
                                "focus": false,
                                "index": "testIndex",
                                "touched": true,
                                "valid": true,
                                "value": "testValue"
                            },
                            "name": "onChange"
                        }
                    ],
                    "state": {
                        "dirty": false,
                        "error": false,
                        "focus": false,
                        "index": "testIndex",
                        "touched": true,
                        "valid": true,
                        "value": "testValue"
                    }
                });
            });
        });
        describe("Given that the component is currently not focussed and was touched", () => {
            function act() {

                const instance = new InputManager({

                    index: "testIndex",
                    value: "testValue",
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    }
                });

                instance.state.touched = true;
                instance.onBlur();
                instance.mockForceUpdate && instance.mockForceUpdate();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "events": [],
                    "state": {
                        "dirty": false,
                        "error": false,
                        "focus": false,
                        "index": "testIndex",
                        "touched": true,
                        "valid": true,
                        "value": "testValue"
                    }
                });
            });
        });
        describe("Given that the component is currently not focussed and was already touched + log onBlur", () => {
            function act() {

                const instance = new InputManager({
                    index: "testIndex",
                    value: "testValue",
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    },
                    debug: {
                        logOnBlur: true
                    }
                });

                instance.state.touched = true;
                instance.onBlur();
                instance.mockForceUpdate && instance.mockForceUpdate();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "events": [
                        {
                            "event": [
                                "testIndex",
                                "onBlur",
                                "Input component does not have focus and was already touched, change not propagated."
                            ],
                            "name": "ConsoleLog"
                        }
                    ],
                    "state": {
                        "dirty": false,
                        "error": false,
                        "focus": false,
                        "index": "testIndex",
                        "touched": true,
                        "valid": true,
                        "value": "testValue"
                    }
                });
            });
        });
    });

    describe("componentDidMount()", () => {
        describe("Given that the component is constructed successfully", () => {
            function act() {
                const instance = new InputManager({
                    index: "testIndex",
                    value: "testValue",
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    }
                });
                instance.componentDidMount();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "events": [
                        {
                            "event": {
                                "dirty": false,
                                "error": false,
                                "focus": false,
                                "index": "testIndex",
                                "touched": false,
                                "valid": true,
                                "value": "testValue"
                            },
                            "name": "onChange"
                        }
                    ],
                    "state": {
                        "dirty": false,
                        "error": false,
                        "focus": false,
                        "index": "testIndex",
                        "touched": false,
                        "valid": true,
                        "value": "testValue"
                    }
                });
            });
        });
        describe("When no onChange prop is passed", () => {
            function act() {
                const instance = new InputManager({
                    ...InputManager.defaultProps,
                    index: "testIndex",
                    value: "testValue",
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                });
                instance.componentDidMount();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Then will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "events": [],
                    "state": {
                        "dirty": false,
                        "error": false,
                        "focus": false,
                        "index": "testIndex",
                        "touched": false,
                        "valid": true,
                        "value": "testValue"
                    }
                });
            });
        });
    });

    describe("componentDidUpdate()", () => {
        describe("When the parent changed the value", () => {
            function act() {
                const instance = new InputManager({

                    index: "testIndex",
                    value: "testValue",
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    }
                });
                instance.props.value = "newTestValue";
                instance.componentDidUpdate({}, {value: "testValue"});
                instance.mockForceUpdate();
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "events": [
                        {
                            "event": {
                                "nextMockState": {
                                    "dirty": true,
                                    "error": false,
                                    "focus": false,
                                    "index": "testIndex",
                                    "touched": false,
                                    "valid": true,
                                    "value": "newTestValue"
                                },
                                "stateUpdate": {
                                    "dirty": true,
                                    "error": false,
                                    "focus": false,
                                    "index": "testIndex",
                                    "touched": false,
                                    "valid": true,
                                    "value": "newTestValue"
                                }
                            },
                            "name": "ReactAddState"
                        },
                        {
                            "event": {
                                "dirty": true,
                                "error": false,
                                "focus": false,
                                "index": "testIndex",
                                "touched": false,
                                "valid": true,
                                "value": "newTestValue"
                            },
                            "name": "onChange"
                        }
                    ],
                    "state": {
                        "dirty": true,
                        "error": false,
                        "focus": false,
                        "index": "testIndex",
                        "touched": false,
                        "valid": true,
                        "value": "newTestValue"
                    }
                });
            });
        });
        describe("When the value didn't change", () => {
            function act() {
                const instance = new InputManager({

                    index: "testIndex",
                    value: "testValue",
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                    onChange: (childState) => {
                        addEvent("onChange", childState);
                    }
                });
                instance.componentDidUpdate({value: "testValue"}, {value: "testValue"});
                return {
                    state: instance.state,
                    events
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse).toEqual({
                    "events": [],
                    "state": {
                        "dirty": false,
                        "error": false,
                        "focus": false,
                        "index": "testIndex",
                        "touched": false,
                        "valid": true,
                        "value": "testValue"
                    }
                });
            });
        });
    });

    describe("render()", () => {
        describe("When only defaultProps are provided", () => {
            function act() {
                const instance = new InputManager(InputManager.defaultProps);
                instance.render();
            }

            test("Then it will throw an error", () => {
                expect(act).toThrow(INPUT_MANAGER_INVALID_INPUT_PROPS);
            });
        });
        describe("When no props are provided except the Input", () => {
            function act() {
                const instance = new InputManager({
                    ...InputManager.defaultProps,
                    Input: () => {
                    }
                });
                const InputReactElement = instance.render();
                return {
                    props: InputReactElement.props,
                    instance
                };
            }

            test("Then these props are passed down to the Input", () => {
                const response = act();
                expect(response.props).toEqual({
                    "dirty": false,
                    "error": false,
                    "focus": false,
                    "onBlur": expect.any(Function),
                    "onChange": expect.any(Function),
                    "onFocus": expect.any(Function),
                    "touched": false,
                    "valid": true,
                    "value": ""
                });
            });
        });
        describe("When no props are provided except the children", () => {
            function act() {
                const instance = new InputManager({
                    ...InputManager.defaultProps,
                    children: (props) => ({props})
                });
                const InputReactElement = instance.render();
                return {
                    props: InputReactElement.props,
                    instance
                };
            }

            test("Then these props are passed down to the children", () => {
                const response = act();
                expect(response.props).toEqual({
                    "dirty": false,
                    "error": false,
                    "focus": false,
                    "onBlur": expect.any(Function),
                    "onChange": expect.any(Function),
                    "onFocus": expect.any(Function),
                    "touched": false,
                    "valid": true,
                    "value": ""
                });
            });
        });
        describe("When all props are provided that the render uses and one prop that it doesn't use", () => {
            function act() {
                const instance = new InputManager({
                    ...InputManager.defaultProps,
                    Input: () => null,
                    onChange: () => null,
                    initialValue: "testValue",
                    validator: () => ({valid: true, error: false}),
                    value: "testValue",
                    children: (props) => ({props}),
                    name: "testName",
                    index: "testIndex",
                    debug: {
                        logOnRender: true
                    },
                    otherProp: "testOtherProp"
                });
                const InputReactElement = instance.render();
                addEvent("PropsToChildren", InputReactElement.props);
                return events;
            }

            test("Then these events are triggered", () => {
                const events = act();
                expect(events).toEqual([{
                    "name": "ConsoleLog",
                    "event": ["testName", {
                        "value": "testValue",
                        "name": "testName",
                        "index": "testIndex",
                        "initialValue": "testValue",
                        "debug": {"logOnRender": true},
                        "otherProp": "testOtherProp",
                        "Input": expect.any(Function),
                        "children": expect.any(Function),
                        "onChange": expect.any(Function),
                        "validator": expect.any(Function),
                    }]
                }, {
                    "name": "PropsToChildren",
                    "event": {
                        "debug": {"logOnRender": true},
                        "otherProp": "testOtherProp",
                        "name": "testName",
                        "index": "testIndex",
                        "value": "testValue",
                        "focus": false,
                        "touched": false,
                        "dirty": false,
                        "valid": true,
                        "error": false,
                        "onBlur": expect.any(Function),
                        "onFocus": expect.any(Function),
                        "onChange": expect.any(Function)
                    }
                }]);
            });
        });
        describe("When no props are provided except the Input, index and logging render", () => {
            function act() {
                const instance = new InputManager({
                    ...InputManager.defaultProps,
                    Input: () => {
                    },
                    index: "testIndex",
                    debug: {
                        logOnRender: true
                    }
                });
                instance.render();
                return events;
            }

            test("Then the index is added to the console.log", () => {
                const response = act();
                expect(response).toEqual([
                    {
                        "name": "ConsoleLog",
                        "event": ["testIndex", {
                            "index": "testIndex",
                            "debug": {"logOnRender": true},
                            "Input": expect.any(Function),
                            "initialValue": "",
                            "onChange": expect.any(Function),
                            "validator": expect.any(Function),
                            "value": ""
                        }]
                    }
                ]);
            });
        });
    });
});
