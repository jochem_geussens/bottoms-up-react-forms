import {InputGroupManager} from "../InputGroupManager";

import {mockReact} from "../../testHelpers/mockReact";
import {addEvent, clearEvents, events} from "jest-mock-utils";
import {
    INPUT_GROUP_MANAGER_INVALID_FORM_PROPS,
    INPUT_GROUP_MANAGER_INVALID_INDEX_PROPS, INPUT_GROUP_MANAGER_INVALID_NAME_PROPS,
    INPUT_GROUP_MANAGER_NO_NAME_IN_BUILD_PROPS
} from "../../helpers/ERRORS";
import {mockConsole} from "../../testHelpers/mockConsole";
import {InputGroupArrayHelper} from "../../helpers/InputGroupArrayHelper";

mockReact();
mockConsole();

const mount = (props) => {
    return new InputGroupManager({...InputGroupManager.defaultProps, ...props});
};

describe("When an instance of the InputGroupManager is created", () => {
    beforeEach(() => {
        clearEvents();
    });
    describe("And the valueType is 'array'", () => {
        const valueType = "array";
        describe("constructor()", () => {
            function act() {
                return mount({
                    valueType
                });
            }

            test("Then the instance will have these values", () => {
                expect(act()).toEqual({
                    "buildProps": expect.any(Function),
                    "helpers": {
                        "array": expect.any(InputGroupArrayHelper),
                        "buildProps": expect.any(Function)
                    },
                    "onChange": expect.any(Function),
                    "props": {
                        "valueType": valueType,
                        "debug": false,
                        "onChange": expect.any(Function),
                        "reducer": expect.any(Function),
                    },
                    "refs": {},
                    "setStateAndOnChange": expect.any(Function),
                    "state": {
                        "childStates": []
                    },
                    "updater": expect.anything()
                });
            });
        });
        describe("buildProps({index, keyGenerator}", () => {

            function act() {
                const instance = mount({
                    valueType: valueType,
                    value: [
                        "testValue1",
                        "testValue2",
                    ],
                    initialValue: [
                        "testInitialValue1",
                        "testInitialValue2",
                    ]

                });
                return {
                    instance,
                    buildProps: instance.buildProps({index: 0})
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse.buildProps).toEqual({
                    value: "testValue1",
                    initialValue: "testInitialValue1",
                    key: "inputGroupKey0",
                    index: 0,
                    name: undefined,
                    onChange: actResponse.instance.onChange
                });
            });
        });
        describe("onChange(childState)", () => {
            describe("Given valid props", () => {
                function act() {
                    const instance = mount({
                        valueType,
                        value: [
                            "testValue1",
                            "testValue2",
                        ],
                        initialValue: [
                            "testInitialValue1",
                            "testInitialValue2",
                        ]
                    });
                    instance.onChange({
                        index: 0,
                        value: "newTestValue1"
                    });
                    instance.onChange({
                        index: 1,
                        value: "newTestValue2"
                    });
                    instance.mockForceUpdate();
                    return {
                        state: instance.state,
                        events
                    };
                }

                test("Will return these values", () => {
                    const actResponse = act();
                    expect(actResponse).toEqual({
                        state: {
                            "childStates": [
                                {"index": 0, "value": "newTestValue1"},
                                {"index": 1, "value": "newTestValue2"}
                            ]
                        },
                        events: [{
                            "event": {
                                "stateUpdate": {
                                    "childStates": [
                                        {"index": 0, "value": "newTestValue1"},
                                        {
                                            "dirty": false,
                                            "error": false,
                                            "focus": false,
                                            "touched": false,
                                            "index": 1,
                                            "valid": true,
                                            "value": "testValue2"
                                        }
                                    ]
                                },
                                "nextMockState": {
                                    "childStates": [
                                        {"index": 0, "value": "newTestValue1"},
                                        {
                                            "dirty": false,
                                            "error": false,
                                            "focus": false,
                                            "index": 1,
                                            "valid": true,
                                            "touched": false,
                                            "value": "testValue2"
                                        }
                                    ]
                                }
                            },
                            "name": "ReactAddState"
                        }, {
                            "event": {
                                "stateUpdate": {
                                    "childStates": [
                                        {"index": 0, "value": "newTestValue1"},
                                        {"index": 1, "value": "newTestValue2"}
                                    ]
                                },
                                "nextMockState": {
                                    "childStates": [
                                        {"index": 0, "value": "newTestValue1"},
                                        {"index": 1, "value": "newTestValue2"}
                                    ]
                                }
                            },
                            "name": "ReactAddState"
                        }]
                    });
                });
            });

            describe("Given missing index with logOnChange set to true", () => {

                function act() {
                    const instance = mount({
                        valueType,
                        debug: {
                            logOnChange: true
                        }
                    });
                    instance.onChange({
                        value: "newTestValue1"
                    });
                }

                test("Will throw this error", () => {
                    expect(act).toThrow(INPUT_GROUP_MANAGER_INVALID_INDEX_PROPS);
                });

                test("Will have logged these values", () => {
                    try {
                        act();
                    } catch (err) { // eslint-disable-line
                    }
                    expect(events).toEqual([
                        {"name": "ConsoleLog", "event": [undefined, {"value": "newTestValue1"}, []]}
                    ]);
                });
            });
        });
    });

    describe("And the valueType is not 'array'", () => {
        const valueType = null;
        describe("constructor()", () => {
            function act() {
                return mount({
                    valueType
                });
            }

            test("Then the instance will have these values", () => {
                expect(act()).toEqual({
                    "buildProps": expect.any(Function),
                    "helpers": {
                        "array": null,
                        "buildProps": expect.any(Function)
                    },
                    "onChange": expect.any(Function),
                    "props": {
                        "valueType": valueType,
                        "debug": false,
                        "onChange": expect.any(Function),
                        "reducer": expect.any(Function)
                    },
                    "setStateAndOnChange": expect.any(Function),
                    "refs": {},
                    "state": {
                        "childStates": {}
                    },
                    "updater": expect.anything()
                });
            });
        });
        describe("buildProps({name, keyGenerator}", () => {

            function act() {
                const instance = mount({
                    valueType,
                    value: {
                        testName1: "testValue1",
                        testName2: "testValue2",
                    },
                    initialValue: {
                        testName1: "testInitialValue1",
                        testName2: "testInitialValue2",
                    }

                });
                return {
                    instance,
                    buildProps: instance.buildProps({name: "testName1"})
                };
            }

            test("Will return these values", () => {
                const actResponse = act();
                expect(actResponse.buildProps).toEqual({
                    value: "testValue1",
                    initialValue: "testInitialValue1",
                    key: "testName1",
                    index: undefined,
                    name: "testName1",
                    onChange: actResponse.instance.onChange
                });
            });
        });
        describe("onChange(childState)", () => {
            describe("Given valid props", () => {
                function act() {
                    const instance = mount({
                        valueType,
                        value: {
                            testName1: "testValue1",
                            testName2: "testValue2",
                        },
                        initialValue: {
                            testName1: "testInitialValue1",
                            testName2: "testInitialValue2",
                        }
                    });
                    instance.onChange({
                        name: "testName1",
                        value: "newTestValue1"
                    });
                    instance.onChange({
                        name: "testName2",
                        value: "newTestValue2"
                    });
                    instance.mockForceUpdate();
                    return {
                        state: instance.state,
                        events
                    };
                }

                test("Will return these values", () => {
                    const actResponse = act();
                    expect(actResponse).toEqual({
                        "state": {
                            "childStates": {
                                "testName1": {"name": "testName1", "value": "newTestValue1"},
                                "testName2": {"name": "testName2", "value": "newTestValue2"}
                            }
                        },
                        "events": [{
                            "name": "ReactAddState",
                            "event": {
                                "stateUpdate": {
                                    "childStates": {
                                        "testName1": {
                                            "name": "testName1",
                                            "value": "newTestValue1"
                                        },
                                        "testName2": {
                                            "value": "testValue2",
                                            "valid": true,
                                            "error": false,
                                            "focus": false,
                                            "touched": false,
                                            "dirty": false,
                                            "name": "testName2"
                                        }
                                    }
                                },
                                "nextMockState": {
                                    "childStates": {
                                        "testName1": {
                                            "name": "testName1",
                                            "value": "newTestValue1"
                                        },
                                        "testName2": {
                                            "value": "testValue2",
                                            "valid": true,
                                            "error": false,
                                            "focus": false,
                                            "touched": false,
                                            "dirty": false,
                                            "name": "testName2"
                                        }
                                    }
                                }
                            }
                        }, {
                            "name": "ReactAddState",
                            "event": {
                                "stateUpdate": {
                                    "childStates": {
                                        "testName1": {
                                            "name": "testName1",
                                            "value": "newTestValue1"
                                        }, "testName2": {"name": "testName2", "value": "newTestValue2"}
                                    }
                                },
                                "nextMockState": {
                                    "childStates": {
                                        "testName1": {
                                            "name": "testName1",
                                            "value": "newTestValue1"
                                        }, "testName2": {"name": "testName2", "value": "newTestValue2"}
                                    }
                                }
                            }
                        }]
                    });
                });

            });
            describe("Given missing name with logOnChange set to true", () => {

                function act() {
                    const instance = mount({
                        valueType,
                        debug: {
                            logOnChange: true
                        }
                    });
                    instance.onChange({
                        value: "newTestValue1"
                    });
                }

                test("Will throw this error", () => {
                    expect(act).toThrow(INPUT_GROUP_MANAGER_INVALID_NAME_PROPS);
                });

                test("Will have logged these values", () => {
                    try {
                        act();
                    } catch (err) { // eslint-disable-line
                    }
                    expect(events).toEqual([
                        {"name": "ConsoleLog", "event": [undefined, {"value": "newTestValue1"}, {}]}
                    ]);
                });
            });
        });
    });

    describe("buildProps()", () => {
        describe("When no name and no index are provided", () => {
            function act() {
                const instance = mount({});
                instance.buildProps({});
                return;
            }

            test("Then an error is thrown", () => {
                expect(act).toThrow(INPUT_GROUP_MANAGER_NO_NAME_IN_BUILD_PROPS);
            });
        });
        describe("When a keyGenerator is provided", () => {
            function act() {
                const instance = mount({dummyProps: "testDummyProps"});
                addEvent("buildProps", instance.buildProps({
                    name: "testName",
                    index: "testIndex",
                    keyGenerator: (args) => {
                        addEvent("KeyGenerator", args);
                        return "myTestKey";
                    }
                }));
                return events;
            }

            test("Then an error is thrown", () => {
                const actResult = act();
                expect(actResult).toEqual([
                    {
                        "name": "KeyGenerator",
                        "event": {
                            "props": {
                                "dummyProps": "testDummyProps",
                                "debug": false,
                                "onChange": expect.any(Function),
                                "reducer": expect.any(Function)
                            }, "name": "testName", "index": "testIndex"
                        }
                    },
                    {
                        "name": "buildProps",
                        "event": {
                            "key": "myTestKey",
                            "index": "testIndex",
                            "name": "testName",
                            onChange: expect.any(Function)
                        }
                    }
                ]);
            });
        });
    });
    describe("shouldComponentUpdate()", () => {
        describe("When the childStates weren't overwritten", () => {
            function act() {
                const instance = mount({});
                instance.shouldComponentUpdate({}, {childStates: instance.state.childStates});
                return instance;
            }

            test("Then the rerender is caused by parent props, and shouldComponentUpdate returns true", () => {
                expect(act()).toBeTruthy();
            });
        });
        describe("When the childStates were overwritten", () => {
            function act() {
                const instance = mount({});
                return instance.shouldComponentUpdate({}, {childStates: {}});
            }

            test("Then the rerender is caused by a child or by the arrayHelper and shouldComponentUpdate " +
                "returns false because the parent will do a setState that does a rerender", () => {
                expect(act()).toBeFalsy();
            });
        });
    });

    describe("render()", () => {
        describe("When no props are provided", () => {
            function act() {
                const instance = mount({});
                const FormReactElement = instance.render();
                return FormReactElement;
            }

            test("Then these props are passed down to the Form", () => {
                expect(act).toThrow(INPUT_GROUP_MANAGER_INVALID_FORM_PROPS);
            });
        });
        describe("When no props are provided except the Form", () => {
            function act() {
                const instance = mount({
                    Group: () => {
                    }
                });
                const FormReactElement = instance.render();
                return {
                    props: FormReactElement.props,
                    instance
                };
            }

            test("Then these props are passed down to the Form", () => {
                const response = act();
                expect(response.props).toEqual({
                    helpers: {
                        array: null,
                        buildProps: expect.any(Function)
                    },
                    onChange: expect.any(Function)
                });
            });
        });
        describe("When no props are provided except the children", () => {
            function act() {
                const instance = mount({
                    children: (props) => ({props})
                });
                const FormReactElement = instance.render();
                return {
                    props: FormReactElement.props,
                    instance
                };
            }

            test("Then these props are passed down to the children", () => {
                const response = act();
                expect(response.props).toEqual({
                    helpers: {
                        array: null,
                        buildProps: expect.any(Function)
                    },
                    onChange: expect.any(Function)
                });
            });
        });
        describe("When all props are provided that the render uses", () => {
            function act() {
                const instance = mount({
                    children: (props) => ({props}),
                    reducer: "testReducer",
                    name: "testName",
                    index: "testIndex",
                    valueType: "testValueType",
                    debug: {
                        logOnRender: true
                    },
                    otherProp: "testOtherProp"
                });
                const FormReactElement = instance.render();
                addEvent("PropsToChildren", FormReactElement.props);
                return events;
            }

            test("Then these events are triggered", () => {
                const events = act();
                expect(events).toEqual([
                    {
                        "event": [
                            "testName",
                            {
                                "children": expect.any(Function),
                                "debug": {
                                    "logOnRender": true
                                },
                                "index": "testIndex",
                                "name": "testName",
                                "otherProp": "testOtherProp",
                                "reducer": "testReducer",
                                "valueType": "testValueType",
                                "onChange": expect.any(Function)
                            }
                        ],
                        "name": "ConsoleLog"
                    },
                    {
                        "event": {
                            helpers: {
                                array: null,
                                buildProps: expect.any(Function)
                            },
                            onChange: expect.any(Function),
                            "otherProp": "testOtherProp"
                        },
                        "name": "PropsToChildren"
                    }
                ]);
            });
        });
        describe("When no props are provided except the Form, index and logging render", () => {
            function act() {
                const instance = mount({
                    Group: () => {
                    },
                    index: "testIndex",
                    debug: {
                        logOnRender: true
                    }
                });
                instance.render();
                return events;
            }

            test("Then the index is added to the console.log", () => {
                const response = act();
                expect(response).toEqual([
                    {
                        "name": "ConsoleLog",
                        "event": ["testIndex", {
                            "index": "testIndex",
                            "debug": {"logOnRender": true},
                            "Group": expect.any(Function),
                            "onChange": expect.any(Function),
                            "reducer": expect.any(Function)
                        }]
                    }
                ]);
            });
        });
    });
});