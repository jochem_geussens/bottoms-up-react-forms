import PropTypes from "prop-types";
import {defaultReducer} from "../helpers/commonReducers";
import React, {Component} from "react";
import {InputGroupArrayHelper} from "../helpers/InputGroupArrayHelper";
import {
    INPUT_GROUP_MANAGER_INVALID_FORM_PROPS,
    INPUT_GROUP_MANAGER_INVALID_INDEX_PROPS,
    INPUT_GROUP_MANAGER_INVALID_NAME_PROPS,
    INPUT_GROUP_MANAGER_NO_NAME_IN_BUILD_PROPS
} from "../helpers/ERRORS";
import {keysChanged} from "../helpers/commonHelpers";
import {ChangeOutput} from "../helpers/ChangeOutput";

export class InputGroupManager extends Component {
    constructor(props) {
        super(props);
        let arrayHelper = null;
        let childStates;
        const initialChildStatesSource = this.props.value || this.props.initialValue;
        if (props.valueType === "array" || Array.isArray(props.value)) {
            childStates = initialChildStatesSource ?
                initialChildStatesSource.map((value, index) => new ChangeOutput({value, index})) :
                [];
            arrayHelper = new InputGroupArrayHelper(this);
        } else {
            childStates = {};
            if (initialChildStatesSource) {
                Object.keys(initialChildStatesSource)
                    .forEach((name) =>
                        childStates[name] = new ChangeOutput({
                            value: initialChildStatesSource[name],
                            name
                        })
                    );
            }
        }
        this.state = {childStates};
        this.onChange = this.onChange.bind(this);
        this.setStateAndOnChange = this.setStateAndOnChange.bind(this);
        this.buildProps = this.buildProps.bind(this);

        // Put the helpers already in an object here so that the this.props.Group can be pure.
        this.helpers = {
            buildProps: this.buildProps,
            array: arrayHelper
        };
    }

    /**
     *
     * @param name
     * @param index
     * @param keyGenerator: recommended when:
     * - the name is not unique
     * - index can not be controlled by the ArrayHelper
     */
    buildProps({name, index, keyGenerator}) {
        const {props, onChange, helpers} = this;
        const {value, initialValue} = props;
        let childStateKey;
        if (typeof index === "number") {
            childStateKey = index;
        } else {
            if (typeof name === "undefined") {
                throw INPUT_GROUP_MANAGER_NO_NAME_IN_BUILD_PROPS;
            }
            childStateKey = name;
        }

        const key = keyGenerator ?
            keyGenerator({props, name, index}) :
            helpers.array ?
                helpers.array.key(childStateKey) :
                childStateKey;
        return {
            value: value && value[childStateKey],
            initialValue: initialValue && initialValue[childStateKey],
            key,
            index,
            name,
            onChange
        };
    }

    setStateAndOnChange(updaterFunction) {
        this.setState(state => {
            const nextState = updaterFunction(state);
            if (!nextState || !nextState.childStates) {
                return null; // ArrayHelper returns no childStates when an invalid action is detected
            }
            const {childStates} = nextState;
            const {name, initialValue, onChange, reducer, index} = this.props;
            onChange(new ChangeOutput(reducer({childStates, name, index, initialValue})));
            return nextState;
        });
    }

    onChange(childState) {
        const {state} = this;
        const {childStates} = state;
        if (this.props.debug && this.props.debug.logOnChange) {
            console.log(this.props.name || this.props.index, childState, childStates); // eslint-disable-line
        }
        if (Array.isArray(childStates)) {
            if (typeof childState.index !== "number") {
                throw INPUT_GROUP_MANAGER_INVALID_INDEX_PROPS;
            }

            this.setStateAndOnChange((nextState) => {
                const newState = {
                    childStates: [...nextState.childStates]
                };
                newState.childStates[childState.index] = childState;
                return newState;
            });
        } else {
            if (typeof childState.name === "undefined") {
                throw INPUT_GROUP_MANAGER_INVALID_NAME_PROPS;
            }
            this.setStateAndOnChange((nextState) => {
                const newState = {
                    childStates: {...nextState.childStates}
                };
                newState.childStates[childState.name] = childState;
                return newState;
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        // Block renders on state change, because this will trigger renders with outdated props.
        // Top level parent should trigger the update
        return !keysChanged(this.state, nextState, ["childStates"]);
    }

    render() {
        const {props, onChange, helpers} = this;
        const {Group, reducer, name, index, valueType, debug, children, ...otherProps} = props; // Filter out props that are only meant for this Component

        if (this.props.debug && this.props.debug.logOnRender) {
            console.log(name || index, props); // eslint-disable-line
        }
        const propsToChildren = {
            ...otherProps,
            onChange,
            helpers
        };
        if (Group) {
            return <Group {...propsToChildren}/>;
        } else if (children) {
            return children(propsToChildren);
        } else {
            throw INPUT_GROUP_MANAGER_INVALID_FORM_PROPS;
        }
    }
}

InputGroupManager.defaultProps = {
    reducer: defaultReducer,
    onChange: () => null,
    debug: false
};

InputGroupManager.propTypes = {
    value: PropTypes.any, // Not required, the children can build the value tree themselves
    Group: PropTypes.oneOfType([PropTypes.element, PropTypes.func]),
    children: PropTypes.oneOfType([PropTypes.func]),
    initialValue: PropTypes.any,
    onChange: PropTypes.func,
    reducer: PropTypes.func.isRequired,
    name: PropTypes.any,
    index: PropTypes.number,
    valueType: PropTypes.oneOf(["array", undefined, false, "object"]),
    debug: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.objectOf({
            logOnChange: PropTypes.bool,
            logOnRender: PropTypes.bool
        })
    ])
};