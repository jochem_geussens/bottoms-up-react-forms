import {arrayHelper} from "./commonHelpers";
import {ARRAY_HELPER_INDEX_LESS_THAN_0, ARRAY_HELPER_INDEX_MORE_THAN_MAX} from "./ERRORS";
import {ChangeOutput} from "./ChangeOutput";

export class InputGroupArrayHelper {
    constructor(inputGroup) {
        this.inputGroup = inputGroup;
        this.keyGeneratorCounter = 0;
        this.keys = [];
        this.add = this.add.bind(this);
        this.deleteAll = this.deleteAll.bind(this);
    }

    add(childState = {value: ""}) {
        // Make the changeOutput here already so that it can be directly called from an onClick event.
        const newChildState = new ChangeOutput({...childState});
        this.inputGroup.setStateAndOnChange((nextState) => {
            const index = nextState.childStates.length;
            const nextChildStates = [...nextState.childStates];
            newChildState.index = index;
            delete newChildState.name;
            nextChildStates.push(newChildState);
            return {childStates: nextChildStates};
        });
    }

    delete(index) {
        if (index < 0) {
            console.log(ARRAY_HELPER_INDEX_LESS_THAN_0); // eslint-disable-line
            return;
        }
        const self = this;
        this.inputGroup.setStateAndOnChange((nextState) => {
            if (index >= nextState.childStates.length) {
                console.log(ARRAY_HELPER_INDEX_MORE_THAN_MAX); // eslint-disable-line
                return {};
            }
            const nextChildStates = [...nextState.childStates];
            nextChildStates.splice(index, 1);
            self.keys.splice(index, 1);
            return {childStates: nextChildStates};
        });
    }

    deleteAll() {
        const self = this;
        this.inputGroup.setStateAndOnChange(() => {
            self.keys.splice(0);
            return {childStates: []};
        });
    }

    moveTo(fromIndex, toIndex) {
        if (fromIndex < 0 || toIndex < 0) {
            console.log(ARRAY_HELPER_INDEX_LESS_THAN_0); // eslint-disable-line
            return;
        }
        const self = this;
        this.inputGroup.setStateAndOnChange((nextState) => {
            if (fromIndex >= nextState.childStates.length || toIndex >= nextState.childStates.length) {
                console.log(ARRAY_HELPER_INDEX_MORE_THAN_MAX); // eslint-disable-line
                return {};
            }
            const nextChildStates = [...nextState.childStates];
            arrayHelper.moveTo(nextChildStates, fromIndex, toIndex);
            arrayHelper.moveTo(self.keys, fromIndex, toIndex);
            return {childStates: nextChildStates};
        });
    }

    swap(index1, index2) {
        if (index1 < 0 || index2 < 0) {
            console.log(ARRAY_HELPER_INDEX_LESS_THAN_0); // eslint-disable-line
            return;
        }
        const self = this;
        this.inputGroup.setStateAndOnChange((nextState) => {
            if (index1 >= nextState.childStates.length || index2 >= nextState.childStates.length) {
                console.log(ARRAY_HELPER_INDEX_MORE_THAN_MAX); // eslint-disable-line
                return {};
            }
            const nextChildStates = [...nextState.childStates];
            arrayHelper.swap(nextChildStates, index1, index2);
            arrayHelper.swap(self.keys, index1, index2);
            return {childStates: nextChildStates};
        });
    }

    key(index) {
        while (this.keys.length < index + 1) {
            this.keys.push("inputGroupKey" + this.keyGeneratorCounter++);
        }
        return this.keys[index];
    }
}