
export function defaultObjectReducer({childStates, initialValue, name, index}) {
    const value = Object.assign({}, initialValue);
    let valid = true;
    let focus = false;
    let touched = false;
    let dirty = false;

    for (const childStateName in childStates) {
        if (!childStates.hasOwnProperty(childStateName)) {
            continue;
        }
        const childState = childStates[childStateName];
        value[childState.name] = childState.value;
        valid = valid && childState.valid;
        focus = focus || childState.focus;
        touched = touched || childState.touched;
        dirty = dirty || childState.dirty;
    }

    return {value, valid, focus, touched, dirty, name, index};
}

export function defaultArrayReducer({childStates, initialValue, name, index}) {
    const value = Array.from(initialValue || []);
    let valid = true;
    let focus = false;
    let touched = false;
    let dirty = false;

    for (let i = 0; i < childStates.length; i++) {
        const childState = childStates[i];
        value[i] = childState.value;
        valid = valid && childState.valid;
        focus = focus || childState.focus;
        touched = touched || childState.touched;
        dirty = dirty || childState.dirty;
    }

    return {value, valid, focus, touched, dirty, name, index};
}

export function defaultReducer({childStates, initialValue, name, index}) {
    if (Array.isArray(childStates)) {
        return defaultArrayReducer({childStates, initialValue, name, index});
    }
    else {
        return defaultObjectReducer({childStates, initialValue, name, index});
    }
}
