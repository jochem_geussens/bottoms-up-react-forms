import {mockReact} from "../../testHelpers/mockReact";

import {InputGroupManager} from "../../components/InputGroupManager";

mockReact();

const mount = (props) => {
    return new InputGroupManager({...InputGroupManager.defaultProps, ...props});
};

describe("When the instance is supplied with valid parameters", () => {
    describe("add()", () => {
        describe("When no arguments are provided", () => {
            function act() {
                const component = mount({valueType: "array"});
                component.helpers.array.add();
                component.mockForceUpdate();
                return component;
            }

            test("Then the childStates will be populated as follows.", () => {
                expect(act().state.childStates).toEqual([{
                    "dirty": false,
                    "error": false,
                    "focus": false,
                    "index": 0,
                    "touched": false,
                    "valid": true,
                    "value": ""
                }]);
            });
        });

        describe("When all arguments are provided in the correct type", () => {
            function act() {
                const component = mount({valueType: "array"});
                component.helpers.array.add({
                    value: "testValue",
                    valid: false,
                    focus: true,
                    dirty: true,
                    touched: true,
                    error: "testError"
                });
                component.mockForceUpdate();
                return component;
            }

            test("Then the childStates will be populated as follows.", () => {
                expect(act().state.childStates).toEqual([{
                    "dirty": true,
                    "error": "testError",
                    "focus": true,
                    "index": 0,
                    "name": undefined,
                    "touched": true,
                    "valid": false,
                    "value": "testValue"
                }]);
            });
        });

        describe("When all arguments are provided as strings", () => {
            function act() {
                const component = mount({valueType: "array"});
                component.helpers.array.add({
                    value: "testValue",
                    valid: "testValid",
                    focus: "testFocus",
                    dirty: "testDirty",
                    touched: "testTouched",
                    error: "testError"
                });
                component.mockForceUpdate();
                return component;
            }

            test("Then the childStates will be populated as follows.", () => {
                expect(act().state.childStates).toEqual([{
                    "dirty": true,
                    "error": false,
                    "focus": true,
                    "index": 0,
                    "name": undefined,
                    "touched": true,
                    "valid": true,
                    "value": "testValue"
                }]);
            });
        });
    });

    describe("delete()", () => {
        describe("When there are 3 items in the childStates with keys assigned", () => {
            function prepare3Items() {
                const component = mount({valueType: "array"});
                component.helpers.array.add({value: "item0"});
                component.helpers.array.add({value: "item1"});
                component.helpers.array.add({value: "item2"});
                component.helpers.array.key(0);
                component.helpers.array.key(1);
                component.helpers.array.key(2);
                component.mockForceUpdate();
                return component;
            }

            describe("And the first one gets deleted", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.delete(0);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will be populated as follows.", () => {
                    expect(act().state.childStates).toEqual([
                        {
                            "dirty": false,
                            "error": false,
                            "focus": false,
                            "index": 1,
                            "touched": false,
                            "valid": true,
                            "value": "item1"
                        },
                        {
                            "dirty": false,
                            "error": false,
                            "focus": false,
                            "index": 2,
                            "touched": false,
                            "valid": true,
                            "value": "item2"
                        }
                    ]);
                });

                test("Then the keys will be populated as follows.", () => {
                    expect(act().helpers.array.keys).toEqual(["inputGroupKey1", "inputGroupKey2"]);
                });
            });

            describe("And the last one gets deleted", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.delete(2);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will be populated as follows.", () => {
                    expect(act().state.childStates).toEqual([
                        {
                            "dirty": false,
                            "error": false,
                            "focus": false,
                            "index": 0,
                            "touched": false,
                            "valid": true,
                            "value": "item0"
                        },
                        {
                            "dirty": false,
                            "error": false,
                            "focus": false,
                            "index": 1,
                            "touched": false,
                            "valid": true,
                            "value": "item1"
                        }
                    ]);
                });

                test("Then the keys will be populated as follows.", () => {
                    expect(act().helpers.array.keys).toEqual(["inputGroupKey0", "inputGroupKey1"]);
                });
            });

            describe("And the index -1 is provided", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.delete(-1);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will remain unchanged.", () => {
                    expect(act().state.childStates).toEqual(prepare3Items().state.childStates);
                });

                test("Then the keys will remain unchanged.", () => {
                    expect(act().helpers.array.keys).toEqual(prepare3Items().helpers.array.keys);
                });
            });

            describe("And the index 3 (out of bound) is provided", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.delete(3);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will remain unchanged.", () => {
                    expect(act().state.childStates).toEqual(prepare3Items().state.childStates);
                });

                test("Then the keys will remain unchanged.", () => {
                    expect(act().helpers.array.keys).toEqual(prepare3Items().helpers.array.keys);
                });
            });
        });
    });

    describe("deleteAll()", () => {
        describe("When there are 3 items in the childStates with keys assigned", () => {
            function prepare3Items() {
                const component = mount({valueType: "array"});
                component.helpers.array.add({value: "item0"});
                component.helpers.array.add({value: "item1"});
                component.helpers.array.add({value: "item2"});
                component.helpers.array.key(0);
                component.helpers.array.key(1);
                component.helpers.array.key(2);
                component.mockForceUpdate();
                return component;
            }

            describe("And the first one gets deleted", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.deleteAll();
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will be populated as follows.", () => {
                    expect(act().state.childStates).toEqual([]);
                });

                test("Then the keys will be populated as follows.", () => {
                    expect(act().helpers.array.keys).toEqual([]);
                });
            });
        });
    });

    describe("moveTo()", () => {
        describe("When there are 3 items in the childStates with keys assigned", () => {
            function prepare3Items() {
                const component = mount({valueType: "array"});
                component.helpers.array.add({value: "item0"});
                component.helpers.array.add({value: "item1"});
                component.helpers.array.add({value: "item2"});
                component.helpers.array.key(0);
                component.helpers.array.key(1);
                component.helpers.array.key(2);
                component.mockForceUpdate();
                return component;
            }

            describe("And the index 0 is moved to index 2", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.moveTo(0, 2);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will be populated as follows.", () => {
                    const originalChildStates = prepare3Items().state.childStates;

                    expect(act().state.childStates).toEqual([
                        originalChildStates[1],
                        originalChildStates[2],
                        originalChildStates[0],
                    ]);
                });

                test("Then the keys will be populated as follows.", () => {
                    expect(act().helpers.array.keys).toEqual(["inputGroupKey1", "inputGroupKey2", "inputGroupKey0"]);
                });
            });

            describe("And the index 2 is moved to index 0", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.moveTo(2, 0);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will be populated as follows.", () => {
                    const originalChildStates = prepare3Items().state.childStates;

                    expect(act().state.childStates).toEqual([
                        originalChildStates[2],
                        originalChildStates[0],
                        originalChildStates[1],
                    ]);
                });

                test("Then the keys will be populated as follows.", () => {
                    expect(act().helpers.array.keys).toEqual(["inputGroupKey2", "inputGroupKey0", "inputGroupKey1"]);
                });
            });

            describe("And the index 1 is moved to index -1", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.moveTo(1, -1);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will remain unchanged.", () => {
                    expect(act().state.childStates).toEqual(prepare3Items().state.childStates);
                });

                test("Then the keys will remain unchanged.", () => {
                    expect(act().helpers.array.keys).toEqual(prepare3Items().helpers.array.keys);
                });
            });

            describe("And the index -1 is moved to index 1", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.moveTo(-1, 1);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will remain unchanged.", () => {
                    expect(act().state.childStates).toEqual(prepare3Items().state.childStates);
                });

                test("Then the keys will remain unchanged.", () => {
                    expect(act().helpers.array.keys).toEqual(prepare3Items().helpers.array.keys);
                });
            });

            describe("And the index 1 is moved to index 3 (out of bounds)", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.moveTo(1, 3);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will remain unchanged.", () => {
                    expect(act().state.childStates).toEqual(prepare3Items().state.childStates);
                });

                test("Then the keys will remain unchanged.", () => {
                    expect(act().helpers.array.keys).toEqual(prepare3Items().helpers.array.keys);
                });
            });

            describe("And the index 3 (out of bounds) is moved to index 1", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.moveTo(3, 1);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will remain unchanged.", () => {
                    expect(act().state.childStates).toEqual(prepare3Items().state.childStates);
                });

                test("Then the keys will remain unchanged.", () => {
                    expect(act().helpers.array.keys).toEqual(prepare3Items().helpers.array.keys);
                });
            });
        });
    });

    describe("swap()", () => {
        describe("When there are 3 items in the childStates with keys assigned", () => {
            function prepare3Items() {
                const component = mount({valueType: "array"});
                component.helpers.array.add({value: "item0"});
                component.helpers.array.add({value: "item1"});
                component.helpers.array.add({value: "item2"});
                component.helpers.array.key(0);
                component.helpers.array.key(1);
                component.helpers.array.key(2);
                component.mockForceUpdate();
                return component;
            }

            describe("And the index 0 is moved to index 2", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.swap(0, 2);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will be populated as follows.", () => {
                    const originalChildStates = prepare3Items().state.childStates;

                    expect(act().state.childStates).toEqual([
                        originalChildStates[2],
                        originalChildStates[1],
                        originalChildStates[0],
                    ]);
                });

                test("Then the keys will be populated as follows.", () => {
                    expect(act().helpers.array.keys).toEqual(["inputGroupKey2", "inputGroupKey1", "inputGroupKey0"]);
                });
            });

            describe("And the index 2 is moved to index 0", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.swap(2, 0);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will be populated as follows.", () => {
                    const originalChildStates = prepare3Items().state.childStates;

                    expect(act().state.childStates).toEqual([
                        originalChildStates[2],
                        originalChildStates[1],
                        originalChildStates[0],
                    ]);
                });

                test("Then the keys will be populated as follows.", () => {
                    expect(act().helpers.array.keys).toEqual(["inputGroupKey2", "inputGroupKey1", "inputGroupKey0"]);
                });
            });

            describe("And the index 1 is moved to index -1", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.swap(1, -1);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will remain unchanged.", () => {
                    expect(act().state.childStates).toEqual(prepare3Items().state.childStates);
                });

                test("Then the keys will remain unchanged.", () => {
                    expect(act().helpers.array.keys).toEqual(prepare3Items().helpers.array.keys);
                });
            });

            describe("And the index -1 is moved to index 1", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.swap(-1, 1);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will remain unchanged.", () => {
                    expect(act().state.childStates).toEqual(prepare3Items().state.childStates);
                });

                test("Then the keys will remain unchanged.", () => {
                    expect(act().helpers.array.keys).toEqual(prepare3Items().helpers.array.keys);
                });
            });

            describe("And the index 1 is moved to index 3 (out of bounds)", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.swap(1, 3);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will remain unchanged.", () => {
                    expect(act().state.childStates).toEqual(prepare3Items().state.childStates);
                });

                test("Then the keys will remain unchanged.", () => {
                    expect(act().helpers.array.keys).toEqual(prepare3Items().helpers.array.keys);
                });
            });

            describe("And the index 3 (out of bounds) is moved to index 1", () => {
                function act() {
                    const component = prepare3Items();
                    component.helpers.array.swap(3, 1);
                    component.mockForceUpdate();
                    return component;
                }

                test("Then the childStates will remain unchanged.", () => {
                    expect(act().state.childStates).toEqual(prepare3Items().state.childStates);
                });

                test("Then the keys will remain unchanged.", () => {
                    expect(act().helpers.array.keys).toEqual(prepare3Items().helpers.array.keys);
                });
            });
        });
    });
});