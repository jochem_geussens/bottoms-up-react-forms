let errorIndex = 1;

export const INPUT_MANAGER_INVALID_INPUT_PROPS = `Error ${errorIndex++}: InputManager expects props to contain a React component named "Input"`;
export const INPUT_MANAGER_INVALID_VALIDATOR_PROPS = `Error ${errorIndex++}: InputManager expects props to contain a function named "validator"`;
export const INPUT_MANAGER_INVALID_VALIDATOR_RESPONSE = `Error ${errorIndex++}: The validator passed to the InputManager did not return an object containing a "value" and "error" property`;

export const INPUT_GROUP_MANAGER_INVALID_FORM_PROPS = `Error ${errorIndex++}: InputGroupManager expects props to contain a React component named "Form" or "children"`;
export const INPUT_GROUP_MANAGER_INVALID_INDEX_PROPS = `Error ${errorIndex++}: InputGroupManager expects child props to contain a React prop named "index" of type number when valueType === array`;
export const INPUT_GROUP_MANAGER_INVALID_NAME_PROPS = `Error ${errorIndex++}: InputGroupManager expects child props to contain a React prop "name" where typeof name !== undefined`;
export const INPUT_GROUP_MANAGER_NO_NAME_IN_BUILD_PROPS = `Error ${errorIndex++}: InputGroupManager expects the helpers.buildProps(...) to receive either an index of type number or a name that is not undefined.`;

export const ARRAY_HELPER_INDEX_LESS_THAN_0 = `Error ${errorIndex++}: InputGroupManager can't move item to less than 0 index. Action was ignored.`;
export const ARRAY_HELPER_INDEX_MORE_THAN_MAX = `Error ${errorIndex++}: InputGroupManager can't move item out of max bounds. Action was ignored.`;