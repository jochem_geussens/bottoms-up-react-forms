export class FormResetter {
    constructor(component) {
        this.component = component;
        this.keyHolder = { // Wrap it so that it can be spread by using '...formResetter.toProps()"
            key: 1
        };
    }

    resetForm() {
        this.keyHolder = {
            key: this.keyHolder.key === 1 ? 2 : 1
        };
        this.component.forceUpdate();
    }

    toProps() {
        return this.keyHolder;
    }

}