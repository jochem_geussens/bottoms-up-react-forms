
export const arrayHelper = {
    moveTo: function(array, fromIndex, toIndex) {
        const valueAtIndex = array.splice(fromIndex, 1);
        const spliced = array.splice(toIndex);
        array.push(...valueAtIndex);
        array.push(...spliced);
        return array;
    },
    swap: function(array, fromIndex, toIndex) {
        const preserve = array[fromIndex];
        array[fromIndex] = array[toIndex];
        array[toIndex] = preserve;
        return array;
    }
};

export function keysChanged(thisPropsOrState, prevPropsOrState, keys) {
    for (let key of keys) {
        if (thisPropsOrState[key] !== prevPropsOrState[key]) {
            return true;
        }
    }
    return false;
}