export function validatorBuilder(...validators) {
    return (value) => {
        for (const validator of validators) {
            const error = validator(value);
            if (error) {
                return {
                    error,
                    valid: !error
                };
            }
        }
        return {error: false, valid: true};
    };
}

export function required() {
    return (value) => value ? false : 'required';
}

export function minimum(minLength) {
    return (value) => !Number.isInteger(minLength) || value.length >= minLength ? false : 'minimum';
}

export function maximum(maxLength) {
    return (value) => !Number.isInteger(maxLength) || value.length <= maxLength ? false : 'maximum';
}

export function password(pattern) {
    return (value) => !pattern || pattern.test(value) ? false : 'password';
}

export function passwordStrength(patternMedium, patternStrong) {
    return (value) => {
        if (!patternMedium || !patternStrong) {
            return false;
        }
        if (patternStrong.test(value)) {
            return false;
        }
        return patternMedium.test(value) ? 'mediumPassword' : 'weakPassword';
    };
}

export function email(pattern) {
    return (value) => !pattern || pattern.test(value) ? false : 'email';
}