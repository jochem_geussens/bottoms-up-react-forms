export class ChangeOutput {
    constructor({value, valid = true, focus, touched, dirty, name, index, error}) {
        this.name = name;
        this.index = index;
        this.value = value;
        this.focus = !!focus;
        this.touched = !!touched;
        this.dirty = !!dirty;
        this.valid = !!valid;
        this.error = this.valid ? false : error;
    }
}