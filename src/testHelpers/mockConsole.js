import {addEvent} from 'jest-mock-utils';

function log(...args) {
    addEvent('ConsoleLog', args);
}

export function mockConsole() {
    console.log = log; // eslint-disable-line
}