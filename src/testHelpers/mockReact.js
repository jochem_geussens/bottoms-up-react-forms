import {addEvent} from 'jest-mock-utils';
import {Component} from 'react';

function enqueueSetState(reactComponentInstance, updater, callback) {
    reactComponentInstance.mockForceUpdate = applyNextMockState.bind(null, reactComponentInstance);
    let stateUpdate = null;
    if (typeof updater === 'object') {
        stateUpdate = {...reactComponentInstance.state, ...updater};
    }
    else {
        stateUpdate = updater(
            reactComponentInstance.nextMockState || reactComponentInstance.state,
            reactComponentInstance.props);
    }

    reactComponentInstance.nextMockState = {...reactComponentInstance.state, ...stateUpdate};
    if (!reactComponentInstance.mockCallbackQueue) {
        reactComponentInstance.mockCallbackQueue = [];
    }
    if (callback) {
        reactComponentInstance.mockCallbackQueue.push(callback);
    }
    addEvent('ReactAddState', {
        stateUpdate,
        nextMockState: reactComponentInstance.nextMockState
    });
}

function applyNextMockState(reactComponentInstance) {
    reactComponentInstance.state = reactComponentInstance.nextMockState;
    if (reactComponentInstance.mockCallbackQueue) {
        for (let callback of reactComponentInstance.mockCallbackQueue) {
            callback();
        }
        reactComponentInstance.mockCallbackQueue.splice(0);
    }
}

export function mockReact() {
    const placeHolderComponent = new Component();
    placeHolderComponent.updater.enqueueSetState = enqueueSetState;
}